CXXFLAGS=-g -Wall -std=c++11 

all: toyserver toyclient ztlsoramserver ztlsoramclient ztoramserver ztoramclient

toyserver: toyserver.o pirserver.o
	$(CXX) -Wall -o $@ $^

toyclient: toyclient.o pirclient.o
	$(CXX) -Wall -o $@ $^

ztlsoramserver: ZT_LSORAMserver.o pirserver.o
	$(MAKE) -C ZeroTrace/
	cp ZeroTrace/Sample_App/ZT.hpp .
	$(CXX) -std=c++11 ZT_LSORAMserver.cc pirserver.cc utils.cc -Wall -L=$(CURDIR)/ZeroTrace -lZT -lcrypto -Wl,--rpath=$(CURDIR)/ZeroTrace -o $@

ztlsoramclient: ZT_LSORAMclient.o pirclient.o utils.o
	$(CXX) -Wall -o  $@ $^ -lcrypto

ztoramserver: ZT_ORAMserver.o pirserver.o
	$(MAKE) -C ZeroTrace/
	cp ZeroTrace/Sample_App/ZT.hpp .
	$(CXX) -std=c++11 ZT_ORAMserver.cc pirserver.cc utils.cc -Wall -L=$(CURDIR)/ZeroTrace -lZT -lcrypto -Wl,--rpath=$(CURDIR)/ZeroTrace -o $@

ztoramclient: ZT_LSORAMclient.o pirclient.o utils.o
	$(CXX) -Wall -o  $@ $^ -lcrypto

clean:
	-rm toyserver.o pirserver.o toyclient.o pirclient.o 
	-rm ZT_LSORAMserver.o ZT_LSORAMclient.o
	-rm ZT_ORAMserver.o ZT_ORAMclient.o

veryclean: clean
	-rm toyserver toyclient
	-rm ztlsoramserver ztlsoramclient
