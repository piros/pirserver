#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <string>
#include <cstdint>
#include <random>
#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/ecdsa.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/obj_mac.h>


#define HASH_LENGTH 32
#define NONCE_LENGTH 16
#define KEY_LENGTH 16
#define MILLION 1E6
#define IV_LENGTH 12
#define EC_KEY_SIZE 32
#define KEY_LENGTH 16
#define TAG_SIZE 16
#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)
#define AES_GCM_BLOCK_SIZE_IN_BYTES 16
#define PRIME256V1_KEY_SIZE 32

#define BLINDED_KEY_SIZE 32
// (Largest value size we have seen so far was 14200)
#define DESCRIPTOR_MAX_SIZE 16384

using namespace std;


int AES_GCM_128_encrypt (unsigned char *plaintext, int plaintext_len, unsigned char *aad,
  int aad_len, unsigned char *key, unsigned char *iv, int iv_len,
  unsigned char *ciphertext, unsigned char *tag);

int AES_GCM_128_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *aad,
  int aad_len, unsigned char *tag, unsigned char *key, unsigned char *iv,
  int iv_len, unsigned char *plaintext);

int32_t serializeLSORAMRequest(unsigned char *key, uint32_t key_size,
         unsigned char *value, uint32_t value_size, unsigned char** serialized_request);

int ECDH_encrypt(const string &plain_request, uint32_t request_size,const string &params, string &shared_secret, string &encrypted_request); 
