#ifndef __PIRCLIENT_H__
#define __PIRCLIENT_H__

#include <string>

using std::string;

// Derive a class from this, implementing the virtual functions, and
// call mainloop().

class PIRClient {
public:
    // Create a PIR query.  The plainquery must be exactly 32 bytes
    // long.
    virtual void create(const string &plainquery, const string &params,
        void *&queryid, string &pirquery) = 0;

    // Extract the plaintext response from a PIR response.  Returns
    // true if successful, false if unsuccessful.
    virtual bool extract(void *&queryid, const string &pirresponse,
        string &plainresponse) = 0;

    // Call this to run the client, and exit when it returns
    void mainloop();
};

#endif
