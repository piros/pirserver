#ifndef __PIRSERVER_H__
#define __PIRSERVER_H__

#include <string>

using std::string;

// Derive a class from this, implementing the virtual functions, and
// call mainloop().

class PIRServer {
public:
    // Fill the current params into the passed string argument
    virtual void get_params(string &params) = 0;

    // Store the given value at the given key.  If the value is the
    // empty string, delete the key.  If the key has already been
    // stored, overwrite the value with this one.  The key will be
    // exactly 32 bytes long.
    virtual void store(const string &key, const string &value) = 0;

    // Perform a private lookup.  The client's private query message is
    // lookup_query.  If successful, return true and fill
    // lookup_response with the private response.  If unsuccessful,
    // return false.
    virtual bool lookup(const string &lookup_query,
                        string &lookup_response) = 0;

    // Call this to run the server, and exit when it returns
    void mainloop();
};

#endif
