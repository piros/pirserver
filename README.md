To build the pirserver and pirclient modules:

1. Clone the piros/Scripts repository (https://git-crysp.uwaterloo.ca/piros/Scripts) and build the dockers in it following instructions in that repository.
2. Build the pirserver docker with `./build_docker.sh`
3. Run the pirserver docker with `./run_docker.sh`

