#include "pirclient.h"

// This is a toy "private" (not really private) lookup protocol.
// It is only here to show you how to implement a subclass of PIRClient.
// Do not use this code for anything requiring actual privacy.
//
// The params are a random 32-byte string.  Lookups are done by
// "encrypting" the lookup key by XORing it with the param string.
// The reply is XORed with repeated copies of the same string.

class ToyClient : public PIRClient {
public:
    ToyClient();

    // Create a PIR query.  The plainquery must be exactly 32 bytes
    // long.
    virtual void create(const string &plainquery, const string &params,
        void *&queryid, string &pirquery);

    // Extract the plaintext response from a PIR response.  Returns
    // true if successful, false if unsuccessful.
    virtual bool extract(void *&queryid, const string &pirresponse,
        string &plainresponse);
};

ToyClient::ToyClient()
{
}

// Put anything you'll need to decrypt the response in here
struct Decryptstate {
    string decryptkey;
};

void
ToyClient::create(const string &plainquery, const string &params,
        void *&queryid, string &pirquery)
{
    if (plainquery.length() == 32 && params.length() == 32) {
        pirquery.assign(plainquery);
        for (size_t i=0;i<32;++i) {
            pirquery[i] ^= params[i];
        }
        Decryptstate *ds = new Decryptstate();
        ds->decryptkey.assign(params);
        queryid = ds;
    }
}

bool
ToyClient::extract(void *&queryid, const string &pirresponse,
        string &plainresponse)
{
    Decryptstate *ds = (Decryptstate *)queryid;
    size_t response_size = pirresponse.length();
    plainresponse.assign(pirresponse);
    for(size_t i=0;i<response_size;++i) {
        plainresponse[i] ^= ds->decryptkey[i % 32];
    }
    delete ds;
    queryid = NULL;

    return true;
}

int main(int argc, char **argv) {
    ToyClient client;

    client.mainloop();

    return 0;
}
