#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <iostream>
#include <map>
#include <iterator>

#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/ecdsa.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/obj_mac.h>

#include "pirserver.h"
#include "ZeroTrace/Globals.hpp"
#include "utils.h"
#include "ZT.hpp"


#define OBLIVIOUS_TYPE_LS 0
#define MEM_MODE 0
#define POPULATE_FLAG 0

// SET ORAM TYPE:
// ORAM_TYPE 0 = Path ORAM
//           1 = Circuit ORAM
#define ORAM_TYPE 0

//if(ORAM_TYPE == 0)
  #define STASH_SIZE 150
  #define Z 4
//else:
//  #define STASH_SIZE 10
  //TODO: When tinkering with CircuitORAM Z,
  // lookout for UTILIZATION_PARAMETER in LS
//  #define Z 2

EC_KEY *ENCLAVE_PUBLIC_KEY = NULL;

// ZT Spawn new ORAM command:
// ZT_New(max_blocks, data_size, stash_size, oblivious, recursion_data_size, oram_type, Z);
#define MAX_BLOCKS 100
#define OBLIVIOUS_TYPE_ORAM 1
#define RECURSION_DATA_SIZE 64

//ORAM Index size (uint32_t) so 4 bytes
#define INDEX_SIZE 4
 
//TODO: Fix PathORAM STASH_SIZE parameter
//      Fix CircuitORAM STASH_SIZE parameter
//      Pick STASH_SIZE parameter based on 

class ZT_ORAMServer : public PIRServer {
private:
  string pubkey;
  uint32_t ZT_lsoram_iid;
  uint32_t ZT_oram_iid;
  uint32_t oram_index;
  
public:
  ZT_ORAMServer();
  void initializeZeroTrace();

  virtual void get_params(string &params);
  
  
  virtual void store(const string &key, const string &value);

  //Helper functions for store:
  int encryptLSORAMRequest(EC_KEY* target_public_key, unsigned char *serialized_request,
      uint32_t request_size, unsigned char **encrypted_request, unsigned char **client_pubkey,
      uint32_t *pubkey_size_x, uint32_t *pubkey_size_y, unsigned char **ecdh_aes_key, 
      unsigned char **iv, unsigned char **tag);

  int LSORAM_Insert(unsigned char *key, uint32_t key_size,
      unsigned char* value, uint32_t value_size);

  virtual bool lookup(const string &lookup_query, string &lookup_response);
  
  //Helper functions for lookup:

};


ZT_ORAMServer::ZT_ORAMServer() {
  initializeZeroTrace(); 

  ZT_lsoram_iid = ZT_New_LSORAM(MAX_BLOCKS, BLINDED_KEY_SIZE, INDEX_SIZE, MEM_MODE, OBLIVIOUS_TYPE_LS, POPULATE_FLAG);

  ZT_oram_iid = ZT_New(MAX_BLOCKS, DESCRIPTOR_MAX_SIZE, STASH_SIZE, OBLIVIOUS_TYPE_ORAM, RECURSION_DATA_SIZE, ORAM_TYPE, Z); 

  oram_index = 0;
}

void ZT_ORAMServer::initializeZeroTrace() {
  // Variables for Enclave Public Key retrieval
   
  //fprintf(stderr, "ZT_LSORAMServer: init: Starting initializezerotrace \n");
  uint32_t max_buff_size = PRIME256V1_KEY_SIZE;
  unsigned char bin_x[PRIME256V1_KEY_SIZE], bin_y[PRIME256V1_KEY_SIZE], signature_r[PRIME256V1_KEY_SIZE], signature_s[PRIME256V1_KEY_SIZE];
 
  int8_t ret; 
  ret = ZT_Initialize(bin_x, bin_y, signature_r, signature_s, max_buff_size); 
  //fprintf(stderr, "ZT_LSORAMServer: init: After ZT_Initialize ret = %d  \n",ret);

  EC_GROUP *curve;
  EC_KEY *enclave_verification_key = NULL;
  ECDSA_SIG *sig_enclave = ECDSA_SIG_new();	
  BIGNUM *x, *y, *xh, *yh;
  BN_CTX *bn_ctx = BN_CTX_new();

  if(NULL == (curve = EC_GROUP_new_by_curve_name(NID_X9_62_prime256v1)))
	  fprintf(stderr, "ZT_LSORAM: init: Setting EC_GROUP failed \n");

  EC_POINT *pub_point = EC_POINT_new(curve);
  //Verify the Enclave Public Key
  enclave_verification_key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);
  xh = BN_bin2bn(hardcoded_verification_key_x, PRIME256V1_KEY_SIZE, NULL);
  yh = BN_bin2bn(hardcoded_verification_key_y, PRIME256V1_KEY_SIZE, NULL);
  EC_KEY_set_public_key_affine_coordinates(enclave_verification_key, xh, yh);
  unsigned char *serialized_public_key = (unsigned char*) malloc (PRIME256V1_KEY_SIZE*2);
  memcpy(serialized_public_key, bin_x, PRIME256V1_KEY_SIZE);
  memcpy(serialized_public_key + PRIME256V1_KEY_SIZE, bin_y, PRIME256V1_KEY_SIZE);
	  
  sig_enclave->r = BN_bin2bn(signature_r, PRIME256V1_KEY_SIZE, NULL);
  sig_enclave->s = BN_bin2bn(signature_s, PRIME256V1_KEY_SIZE, NULL);	
  
  ret = ECDSA_do_verify((const unsigned char*) serialized_public_key, PRIME256V1_KEY_SIZE*2, sig_enclave, enclave_verification_key);

  if(ret==1){
	  fprintf(stderr, "ZT_LSORAM: init: GetEnclavePublishedKey : Verification Successful! \n");
  }
  else{
	  fprintf(stderr, "ZT_LSORAM: init: GetEnclavePublishedKey : Verification FAILED! \n");
  }
  
  //Load the Enclave Public Key
  ENCLAVE_PUBLIC_KEY = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);
  
  x = BN_bin2bn(bin_x, PRIME256V1_KEY_SIZE, NULL);
  y = BN_bin2bn(bin_y, PRIME256V1_KEY_SIZE, NULL);
  if(EC_POINT_set_affine_coordinates_GFp(curve, pub_point, x, y, bn_ctx)==0)
	  fprintf(stderr, "ZT_LSORAMServer: EC_POINT_set_affine_coordinates FAILED \n");

  if(EC_KEY_set_public_key(ENCLAVE_PUBLIC_KEY, pub_point)==0)
	  fprintf(stderr, "ZT_LSORAMServer: EC_KEY_set_public_key FAILED \n");

  BN_CTX_free(bn_ctx);
  pubkey.assign((const char*) serialized_public_key, 2*PRIME256V1_KEY_SIZE);
  free(serialized_public_key); 

  fprintf(stderr, "ZT_LSORAMServer: Finished initializezerotrace, pubkey set \n");
}


void ZT_ORAMServer::get_params(string &params) {
  //Populate params with the enclave public key
  fprintf(stderr, "ZT_LSORAMServer: Started get_params(), params length = %ld\n", params.length());
  if(pubkey.empty())
    fprintf(stderr, "ZT_LSORAMServer: pubkey is empty when get_params is called. WHY? \n");
  params.assign(pubkey);

  fprintf(stderr, "ZT_LSORAMServer: Finished get_params(), params.length = %ld,\n", params.length());
  /*
  unsigned char *param_ptr = (unsigned char*) params.c_str();
  fprintf(stderr, "Param in ZT_LSORAMServer::get_params():");
  for(uint32_t i=0;i<params.length();i++){
    fprintf(stderr, "%02x", param_ptr[i]);
  }
  fprintf(stderr, "\n");

  fprintf(stderr, "ZT_LSORAMServer::ZT_instance_id = %d, params length = %ld\n", ZT_instance_id, params.length());
  */
}


/*

Inputs: a target pub key, a seriailzed request and request size.
Outputs: instantiates and populates:
          client_pubkey, aes_key (from target_pubkey and generated client_pubkey ECDH)
          iv, encrypted request and tag for the request
*/
int ZT_ORAMServer::encryptLSORAMRequest(EC_KEY* target_public_key, unsigned char *serialized_request,
     uint32_t request_size, unsigned char **encrypted_request, unsigned char **client_pubkey,
     uint32_t *pubkey_size_x, uint32_t *pubkey_size_y, unsigned char **ecdh_aes_key, 
     unsigned char **iv, unsigned char **tag){

  //Generate a new key
  EC_KEY *ephemeral_key = NULL;
  BIGNUM *x, *y;
  x = BN_new();
  y = BN_new();
  BN_CTX *bn_ctx = BN_CTX_new();
  const EC_GROUP *curve = NULL;

  if(NULL == (curve = EC_GROUP_new_by_curve_name(NID_X9_62_prime256v1)))
    fprintf(stderr, "ZT_LSORAMServer: Setting EC_GROUP failed \n");

  ephemeral_key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);
  if(ephemeral_key==NULL)
    fprintf(stderr, "ZT_LSORAMServer: EC_KEY_new_by_curve_name Fail\n");

  int ret = EC_KEY_generate_key(ephemeral_key);
  if(ret!=1)
    fprintf(stderr, "ZT_LSORAMServer: EC_KEY_generate_key Fail\n");

  const EC_POINT *pub_point;
  pub_point = EC_KEY_get0_public_key((const EC_KEY *) ephemeral_key);
  if(pub_point == NULL)
    fprintf(stderr, "ZT_LSORAMServer: EC_KEY_get0_public_key Fail\n");
      
  ret = EC_POINT_get_affine_coordinates_GFp(curve, pub_point, x, y, bn_ctx);
  if(ret==0)
    fprintf(stderr, "ZT_LSORAMServer: EC_POINT_get_affine_coordinates_GFp Failed \n");
      
  unsigned char *bin_x, *bin_y;
  uint32_t size_bin_x = BN_num_bytes(x);
  uint32_t size_bin_y = BN_num_bytes(y);
  //fprintf(stderr, "ZT_LSORAMServer: (%d, %d)\n", size_bin_x, size_bin_y);
  bin_x = (unsigned char*) malloc(PRIME256V1_KEY_SIZE);
  bin_y = (unsigned char*) malloc(PRIME256V1_KEY_SIZE);
  BN_bn2bin(x, bin_x);
  BN_bn2bin(y, bin_y);
  *pubkey_size_x = size_bin_x;
  *pubkey_size_y = size_bin_y;
  *client_pubkey = (unsigned char*) malloc(size_bin_x + size_bin_y);
  memcpy(*client_pubkey, bin_x, size_bin_x);
  memcpy(*client_pubkey + size_bin_x, bin_y, size_bin_y);

  /*
  unsigned char *ptr = *client_pubkey;
  printf("Serialized Client's Public Key in encryptLSORAM :\n");
  for(int t = 0; t < size_bin_x; t++)
  printf("%02X", ptr[t]);
  printf("\n");
  printf("Serialized Client's Public Key in encryptLSORAM :\n");
  for(int t = 0; t < size_bin_y; t++)
    printf("%02X", ptr[size_bin_x + t]);
  printf("\n");
  */

  //fprintf(stderr, "ZT_LSORAMServer: Before ECDH_compute_key \n");
  uint32_t field_size = EC_GROUP_get_degree(EC_KEY_get0_group(target_public_key));
  uint32_t secret_len = (field_size+7)/8;
  unsigned char *secret = (unsigned char*) malloc(secret_len);
  //Returns a 32 byte secret	
  secret_len = ECDH_compute_key(secret, secret_len, EC_KEY_get0_public_key(target_public_key),
					  ephemeral_key, NULL);

  //fprintf(stderr, "ZT_LSORAMServer: Finished ECDH_compute_key \n");
  //Sample IV;
  *ecdh_aes_key = (unsigned char*) malloc (KEY_LENGTH);
  *iv = (unsigned char*) malloc (IV_LENGTH);
 
  memcpy(*ecdh_aes_key, secret, KEY_LENGTH);
  memcpy(*iv, secret + KEY_LENGTH, IV_LENGTH);

  /*
  unsigned char *ecdh_ptr = (unsigned char *) *ecdh_aes_key;
  unsigned char *iv_ptr = (unsigned char *) *iv;
  
  printf("KEY_LENGTH = %d\n", KEY_LENGTH);
  printf("ecdh_key computed by Client :\n");
  for(int t = 0; t < KEY_LENGTH; t++)
    printf("%02X",  ecdh_ptr[t]);
  printf("\n");
  printf("iv computed by Client :\n");
  for(int t = 0; t < IV_LENGTH; t++)
    printf("%02X", iv_ptr[t]);
  printf("\n");
  */

  BN_CTX_free(bn_ctx);

  *encrypted_request = (unsigned char*) malloc (request_size);
  *tag = (unsigned char*) malloc (TAG_SIZE);

  uint32_t encrypted_request_size;
  /*
  printf("Request bytes before encrypting: \n"); 
  for(int t = 0; t < request_size; t++)
    printf("%02X", serialized_request[t]);
  printf("\n"); 
  */


  //fprintf(stderr, "ZT_LSORAMServer: Before AES_GCM_128_encrypt call \n");
  encrypted_request_size = AES_GCM_128_encrypt(serialized_request, request_size,
           NULL, 0, (unsigned char*) *ecdh_aes_key, (unsigned char*) *iv, 
           IV_LENGTH, *encrypted_request, *tag);

  /*
  unsigned char*tag_ptr = *tag;
  printf("Tag bytes after encryption: \n"); 
  for(uint32_t t = 0; t < TAG_SIZE; t++)
    printf("%02X", tag_ptr[t]);
  printf("\n"); 
 
  printf("Request_size = %d, Encrypted_request_size = %d,\n", request_size, encrypted_request_size); 
  printf("Request bytes after encrypting: \n");
  unsigned char *encrypted_ptr = (unsigned char*) *encrypted_request;
  for(uint32_t t = 0; t < encrypted_request_size; t++)
    printf("%02X",  encrypted_ptr[t]);
  printf("\n"); 
  */
  return encrypted_request_size;
}

int ZT_ORAMServer::LSORAM_Insert(unsigned char *key, uint32_t key_size, unsigned char* value, uint32_t value_size){
  unsigned char *serialized_request, *encrypted_request, *tag_in;
  unsigned char *client_pubkey, *ecdh_aes_key, *iv;
  uint32_t pubkey_size_x, pubkey_size_y;
 
  uint32_t request_size = serializeLSORAMRequest(key, key_size, value, value_size, &serialized_request);

  
  //fprintf(stderr, "ZT_LSORAMServer: Before encryptLSORAMRequest\n");
  encryptLSORAMRequest(ENCLAVE_PUBLIC_KEY, serialized_request, request_size, 
         &encrypted_request, &client_pubkey, &pubkey_size_x, &pubkey_size_y, 
         &ecdh_aes_key, &iv, &tag_in);
  //fprintf(stderr, "ZT_LSORAMServer: After encryptLSORAMRequest\n");
  /* 
  printf("Clientpubkey going into ZT_LSORAM_insert:\n");
  printf("X: :\n");
  for(int t = 0; t < 32; t++)
  printf("%02X", client_pubkey[t]);
  printf("\n");
  printf("Y :\n");
  for(int t = 0; t < 32; t++)
    printf("%02X", client_pubkey[32+t]);
  printf("\n");
  */

  //fprintf(stderr, "ZT_LSORAMServer: Before ZT_LSORAM_insert\n");
  ZT_HSORAM_insert(ZT_lsoram_iid, ZT_oram_iid, ORAM_TYPE, oram_index++, encrypted_request,
     request_size, tag_in, TAG_SIZE, client_pubkey, pubkey_size_x, pubkey_size_y);

  //fprintf(stderr, "ZT_LSORAMServer: After ZT_LSORAM_insert\n");
  free(serialized_request);
  return 1;   
}

void ZT_ORAMServer::store(const string &key, const string &value){

  fprintf(stderr, "ZT_ORAMServer: Started store\n");
  if (value.length() > 0) {
    //Create encrypted request with strings key, value
   
    unsigned char *key_ptr = (unsigned char*) key.c_str();
    unsigned char *value_ptr = (unsigned char*) value.c_str();
    //fprintf(stderr, "ZT_LSORAMServer: Starting store(), key.length= %ld, value.length = %ld\n", key.length(), value.length());

    LSORAM_Insert((unsigned char*) key.c_str(), BLINDED_KEY_SIZE,
          (unsigned char*) value.c_str(), DESCRIPTOR_MAX_SIZE);

    fprintf(stderr, "ZT_ORAMServer: STORED HSDesc Key = ");
    for(uint32_t i = 0; i <32; i++){
      fprintf(stderr, "%02x", key_ptr[i]);
    }    
    fprintf(stderr,"\n");

    fprintf(stderr, "ZT_ORAMServer: STORED (First 32 bytes of ) HSDesc Value= ");
    for(uint64_t i = 0; i < 32; i++){
      fprintf(stderr, "%02x", value_ptr[i]);
    }    
    fprintf(stderr,"\n");

  } else {
    //int8_t ZT_LSORAM_evict(uint32_t id, unsigned char *key, uint32_t key_size);
  }
}


/*
  In ZT_LSORAMClient lookup_query should be:
  encrypted_query||tag_in||pk_x_size||pk_y_size||client_pubkey
  where client_pubkey is of size pk_x_size+pk_y_size

  returns lookup_response:
  encrypted_response||tag_out
*/

bool ZT_ORAMServer::lookup(const string &lookup_query, string &lookup_response) {

  fprintf(stderr, "ZT_ORAMServer: Starting lookup() call\n");
  const char *lookup_query_cstr= lookup_query.c_str();
  fprintf(stderr, "ZT_ORAMServer: lookup_query length = %ld\n", lookup_query.length());
  unsigned char *encrypted_query = (unsigned char*) malloc (BLINDED_KEY_SIZE);
  unsigned char *tag_in = (unsigned char*) malloc (TAG_SIZE);
  uint32_t pk_x_size;
  uint32_t pk_y_size;
  unsigned char *client_pubkey;
  

  fprintf(stderr, "ZT_ORAMServer: Before parsing lookup_query_cstr\n");
  unsigned char *ptr = (unsigned char*) lookup_query_cstr;
  memcpy(encrypted_query, ptr, BLINDED_KEY_SIZE);
  ptr+=BLINDED_KEY_SIZE;
  memcpy(tag_in, ptr, TAG_SIZE);
  ptr+=TAG_SIZE;
  memcpy(&pk_x_size, ptr, sizeof(uint32_t));
  ptr+=sizeof(uint32_t);   
  memcpy(&pk_y_size, ptr, sizeof(uint32_t));
  ptr+=sizeof(uint32_t);   

  size_t expected_size = BLINDED_KEY_SIZE+TAG_SIZE+ 2*sizeof(uint32_t) + 
           pk_x_size + pk_y_size;
  
  fprintf(stderr, "ZT_ORAMServer: Before allocating client_pubkey call (%d,%d)\n", pk_x_size, pk_y_size);
  fprintf(stderr, "ZT_ORAMServer: expected_size = %ld, lookup_query.length = %ld\n", expected_size, lookup_query.length());

  /*
  if(lookup_query.length()!=expected_size)
    fprintf(stderr, "ZT_LSORAMServer: Query size doesn't match KEY_SIZE + TAG_SIZE\n");
    return 0; 
  */

  client_pubkey = (unsigned char*) malloc (pk_x_size+pk_y_size);
  memcpy(client_pubkey, ptr, (pk_x_size + pk_y_size));


  unsigned char *encrypted_response = (unsigned char*) malloc(DESCRIPTOR_MAX_SIZE);
  unsigned char *tag_out = (unsigned char*) malloc(TAG_SIZE);

  fprintf(stderr, "ZT_ORAMServer: Before ZT_LSORAM_fetch() call\n");
  ZT_HSORAM_fetch(ZT_lsoram_iid, ZT_oram_iid, ORAM_TYPE, encrypted_query, BLINDED_KEY_SIZE,
     encrypted_response, DESCRIPTOR_MAX_SIZE, tag_in, tag_out, TAG_SIZE,
     client_pubkey, pk_x_size, pk_y_size);
  fprintf(stderr, "ZT_ORAMServer: After ZT_LSORAM_fetch() call\n");

  lookup_response.assign((const char*) encrypted_response, DESCRIPTOR_MAX_SIZE);
  lookup_response.append((const char*) tag_out, TAG_SIZE);

  free(encrypted_response);
  free(tag_out);
  free(client_pubkey);
  free(encrypted_query);
  free(tag_in);

  fprintf(stderr, "ZT_ORAMServer: Finished lookup() call\n");
  return 1;
}

int main(int argc, char **argv) {
    ZT_ORAMServer server;

    server.mainloop();

    return 0;
}
